package com.example.quickstart

import cats.Applicative
import cats.effect.{IO, Sync}
import cats.implicits._
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._
import org.http4s._
import org.http4s.implicits._
import org.http4s.{EntityDecoder, EntityEncoder, Method, Request, Uri}
import org.http4s.circe._
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.headers.Accept
import org.http4s.Method._
import cats._
import cats.effect._
import cats.implicits._
import org.http4s.Uri

import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global
import cats.syntax.parallel._

trait Titles[F[_]]{
  def get(pathUri: String): F[Titles.Title]
  def getList(pathUris: List[String]): F[List[Titles.Title]]
}


object Titles {

  def apply[F[_]](implicit ev: Titles[F]): Titles[F] = ev

  final case class Title(name: String, url:String )
  final case class TitleError(e: Throwable) extends RuntimeException

  object Title {
    implicit val titleDecoder: Decoder[Title] = deriveDecoder[Title]
    implicit val titleEncoder: Encoder[Title] = deriveEncoder[Title]

    implicit def titleEntityDecoder[F[_]: Sync]: EntityDecoder[F, Title] = jsonOf
    implicit def titleEntityEncoder[F[_]: Applicative]: EntityEncoder[F, Title] = jsonEncoderOf
  }

  def impl[F[_] : Sync](client: Client[F]): Titles[F] = new Titles[F] {
    val dsl = new Http4sClientDsl[F] {}

    import dsl._

    def get(pathUri: String): F[Titles.Title] = {
      val uriP = Uri.unsafeFromString(pathUri)
      val request = GET(uriP, Accept(MediaType.text.html))
      client.expect[String](request)
        .map(x => Title(Util.getTitleContext(x), pathUri))
    }

    def getList(pathUris: List[String]): F[List[Titles.Title]] = {
       pathUris.map(pathUri => get(pathUri))
         .traverse(identity)
    }
  }

}

