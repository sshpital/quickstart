package com.example.quickstart

import cats.effect.Sync
import cats.implicits._
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.impl.QueryParamDecoderMatcher

object QuickstartRoutes {


  object UrlQueryParamMatcher extends QueryParamDecoderMatcher[String]("url")

  def titleRoutes[F[_]: Sync](titles: Titles[F]): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    HttpRoutes.of[F] {
      case GET -> Root / "title" :? UrlQueryParamMatcher(urlString) =>
        for {
          title <- titles.get(urlString)
          resp <- Ok(title)
        } yield resp

      case GET -> Root   =>  Ok("\n\n GET -> localhost:8080/title?url=http://google.com\n\nPOST-> localhost:8080/titles   body =   "+
      """
          |[
          |	{ "name": "http://gmail.com" },
          |	{ "name": "http://gmail.com" },
          |	{ "name": "http://gmail.com" },
          |	{ "name": "http://gmail.com" }
          |]
          |""".stripMargin)
    }
  }

  case class Ur(name: String)

  def titlesRoutes[F[_] : Sync](titles: Titles[F]): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    import io.circe.generic.auto._
    import org.http4s.circe.CirceEntityCodec._

    HttpRoutes.of[F] {
      case req@POST -> Root / "titles" => {
        for {
          //
          ur <- req.as[List[Ur]]
          names = ur.map(x => x.name)
          pl <- titles.getList(names)
          r <- Ok(pl)
        } yield r
      }
    }
  }
}

