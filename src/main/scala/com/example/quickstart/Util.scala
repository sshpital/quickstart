package com.example.quickstart

import scala.util.matching
import scala.util.matching.Regex

object Util {

  def getTitleContext(text: String): String = {
    val titleRegex: Regex = raw"(?i)<title>(.*?)</title>".r
    titleRegex.findFirstMatchIn(text) match {
      case Some(titleRegex(t)) => t
      case None => ""
    }
  }

}
