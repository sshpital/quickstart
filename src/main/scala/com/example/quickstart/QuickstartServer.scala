package com.example.quickstart

import cats.effect.{ConcurrentEffect, ContextShift, Timer}
import cats.implicits._
import fs2.Stream
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.client.middleware.FollowRedirect
import org.http4s.headers.{AgentProduct, `User-Agent`}
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.Logger

import scala.concurrent.ExecutionContext.global

object QuickstartServer {

  def stream[F[_]: ConcurrentEffect](implicit T: Timer[F], C: ContextShift[F]): Stream[F, Nothing] = {

    for {

      client0 <- BlazeClientBuilder[F](global)
        .withUserAgent(`User-Agent`(AgentProduct("http4s based application (banderlog41@mail.ru)")))
        .stream

      client = FollowRedirect(maxRedirects = 5)(client0)

      titleAlg = Titles.impl[F](client)

      httpApp = (
         QuickstartRoutes.titleRoutes[F](titleAlg) <+>
           QuickstartRoutes.titlesRoutes[F](titleAlg)
        ).orNotFound

      // With Middlewares in place
      finalHttpApp = Logger.httpApp(true, true)(httpApp)

      exitCode <- BlazeServerBuilder[F](global)
        .bindHttp(8080, "0.0.0.0")
        .withHttpApp(finalHttpApp)
        .serve
    } yield exitCode

  }.drain
}
